## How to launch the game
There are two ways to play the game. (hjemmelaget video som viser kort oppstart og spill: https://youtu.be/22msifjtdOE )

### Option 1: <p>
Download java if you do not already have it installed. Download the source code of the project as a zip and unpack it.
For Windows users: Simply launch the file named "start" in the main folder.
For Mac and others: Open a terminal and navigate to the main folder of the game and type the command "java -jar Dodgejar.jar"

There is a .exe that is not working properly, ignore this.

### Option 2: <p>
If you wish to play the game via a code editor, you will have to download a code editor that supports java.
Follow this link and do steps 1-3 before you continue: https://git.app.uib.no/Mathias.H.Hellum/lab0 <p>
Navigate either to the main folder and open Git or a terminal here or navigate to your code editors terminal.
Fork the repository and copy the SSH key found under "Clone".
Use the command "git clone *insert here*" in your editors terminal.
You can now launch the game via running the file "Main.java" <p>
The advantage of doing option 2 is that you can freely and more easily inspect the code and make changes.

## Game instructions for Dodge
Instructions for how to play the game can be found under INFO when launching the game.

The objective of the game is to dodge the enemy characters that spawn routinely.<p>
There are two difficulties, normal and hard, each with a different spawn schedule for enemies.
The enemies have different behaviours, some are smart and follow you while others might be unpredictable.<p>
There is an in game store where you can purchase items to help you survive, each with different effects.
If your health reaches 0, you lose. To help stay alive you can purchase shields to mitigate damage to your health.

Will you find out how to win?