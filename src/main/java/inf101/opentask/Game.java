package inf101.opentask;

import inf101.opentask.assetManagement.AudioPlayer;
import inf101.opentask.assetManagement.BufferedImageLoader;
import inf101.opentask.controls.KeyInput;
import inf101.opentask.controls.MouseInput;
import inf101.opentask.entities.EntityID;
import inf101.opentask.entities.objects.MenuParticle;
import inf101.opentask.uiElements.Hud;
import inf101.opentask.uiElements.Menu;
import inf101.opentask.uiElements.Store;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.util.Random;

public class Game extends Canvas implements Runnable {

    /*
    Flere kilder brukt for å forstå  bruk av javas Runnable interface og threads generelt:
    https://www.youtube.com/watch?v=UXW5a-iHjso&ab_channel=ProgrammingKnowledge
    https://www.youtube.com/watch?v=8bSlaGsG4dk&ab_channel=ProgrammingKnowledge
    https://www.oreilly.com/library/view/java-for-dummies/9781118239742/a66_06_9781118239742-ch03.html
    */

    // The games' aspect ratio is 4/3
    public static final int WIDTH = 800, HEIGHT = 600;
    private Thread thread;
    private boolean running = false;
    // Gives the option to have endless difficulty modes / spawn templates
    // 0 == normal
    // 1 == hard
    public int gameDifficulty = 0;

    private final Random r;
    private final Handler handler;
    private final Hud hud;
    private final RoundManager spawner;
    private final Menu menu;
    private final Store store;
    // Source path strings are not local variables due to ease of access and readability
    private final String bgm = "src/main/resources/bgm.wav";
    private final String sprites = "/sprite-sheet.png";
    private final String spritesLarge = "/sprite-sheet_large.png";
    private GameScreen gameScreen;
    private MouseInput mouseInput;
    // BufferedImage objects are fine as public statics as they are never modified
    public static BufferedImage spriteSheet;
    public static BufferedImage spriteSheetLarge;

    public Game(){
        BufferedImageLoader loader = new BufferedImageLoader();
        spriteSheet = loader.loadImage(sprites);
        BufferedImageLoader bossLoader = new BufferedImageLoader();
        spriteSheetLarge = bossLoader.loadImage(spritesLarge);
        handler = new Handler();
        hud = new Hud();
        MouseInput mouseInput = new MouseInput(this, hud, handler);
        store = new Store(hud, mouseInput);
        menu = new Menu(this, hud);
        this.addMouseListener(store);
        this.addKeyListener(new KeyInput(handler, this));
        this.addMouseListener(mouseInput);
        AudioPlayer audioPlayer = new AudioPlayer();
        audioPlayer.playMusic(bgm);
        spawner = new RoundManager(handler, hud, this);
        r = new Random();
        this.gameScreen = GameScreen.WELCOME;
        // Menu particle effects added when game is launched
        for (int i = 0; i < 20; i++) {
            handler.addObject(new MenuParticle(r.nextInt(Game.WIDTH), r.nextInt(Game.HEIGHT), EntityID.MENU_PARTICLE, handler));
        }
        this.mouseInput = mouseInput;
    }

    /**
     * The method that starts a thread
     */
    public synchronized void start(){
        thread = new Thread(this);
        thread.start();
        running = true;
    }

    /**
     * The method that ends a thread.
     * Done to avoid having a running thread after closing the application.
     */
    public synchronized void stop(){
        try {
            thread.join();
            running = false;
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * The native run function called by the Runnable interface. Locked to 60 FPS.
     * <p>
     * Drift adjusting.
     */
    public void run(){
        // In case the application is not in focus
        this.requestFocus();
        /*
        The use of delta and its related code prevents potential drifting that can occur while running the program.
        To have 60 fps we want render & tick to be called every 1000 / 60 ms.
        If frames drop to 58 fps, delta will be 2 instead of 1 at 60 fps, thus calling tick twice to compensate.
        Ideally delta would always be 1, this is not always the case with threads, resulting in drifting.
        If the thread and the ticks are not synced, there won't be any loss of ticks as it adjusts delta and ticks to match.

            NB!!
            Tipset om å gjøre dette har jeg delvis fått av en venn utenfor studiet,
            men har også sett forslaget i forskjellige kommentarfelt på youtubevideor,
            har derfor ikke en konkret kilde å referere til utenom å nevne at det ikke er mitt verk.
         */
        long lastTime = System.nanoTime();
        double amountOfTicks = 60.0;
        double ns = 1000000000 / amountOfTicks;
        double delta = 0;
        while (running) {
            // Puts the thread to sleep between ticks to avoid unnecessary load on CPU
            try {
                Thread.sleep(1000/60);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            long now = System.nanoTime();
            delta+= (now - lastTime) / ns;
            lastTime = now;
            while (delta >= 1){
                tick();
                render();
                delta--;
            }
        }
        stop();
    }

    /**
     * The tick method for the game itself.
     * Calls on relevant tick methods in different GameScreens
     */
    private void tick(){
        if(gameScreen == GameScreen.ACTIVE_GAME){
            hud.tick();
            spawner.tick();
            handler.tick();
            // What happens when the player dies
            if(hud.health <= 0){
                setGameScreen(GameScreen.GAME_OVER);
                hud.health = 100;
                spawner.ROUND_1 = !spawner.ROUND_1;
                handler.object.clear();
                // The same particle effects added at game over as in the menu
                for (int i = 0; i < 20; i++) {
                    handler.addObject(new MenuParticle(r.nextInt(Game.WIDTH), r.nextInt(Game.HEIGHT), EntityID.MENU_PARTICLE, handler));
                }
            }
            if(mouseInput.itemFourBoughtOut){
                setGameScreen(GameScreen.VICTORY);
                hud.health = 100;
                hud.shield = 0;
                spawner.ROUND_1 = !spawner.ROUND_1;
                handler.object.clear();
                for (int i = 0; i < 20; i++) {
                    handler.addObject(new MenuParticle(r.nextInt(Game.WIDTH), r.nextInt(Game.HEIGHT), EntityID.MENU_PARTICLE, handler));
                }
            }
        }
        else if(gameScreen == GameScreen.WELCOME || gameScreen == GameScreen.INFO || gameScreen == GameScreen.GAME_OVER || gameScreen == GameScreen.SELECT_DIFFICULTY || gameScreen == GameScreen.VICTORY){
            handler.tick();
        }
    }

    /**
     * The render method for the Game.
     * Calls on relevant render methods in different GameScreens.
     * <p>
     * Uses a BufferStrategy to have a few buffers of what is to be rendered
     */
    private void render(){
        /*
        Noen av kildene for å forstå BufferStrategy:
        https://stackoverflow.com/questions/13590002/understand-bufferstrategy
        https://www.youtube.com/watch?v=nzHDQvzUXL0&ab_channel=thenewboston
         */
        // Gets the BufferStrategy used
        BufferStrategy bufferStrategy = this.getBufferStrategy();
        // If there is no BufferStrategy, creates an image/buffer off-screen that can be "called" later
        if(bufferStrategy == null){
            this.createBufferStrategy(3); // Has to be more than 1 but not many needed
            return;
        }

        // Draws the whole background by using a native method to BufferStrategy
        Graphics g = bufferStrategy.getDrawGraphics();
        g.setColor(Color.BLACK);
        g.fillRect(0, 0, WIDTH, HEIGHT);

        // If in-game
        if(gameScreen == GameScreen.ACTIVE_GAME){
            handler.render(g);
            hud.render(g);
        }

        // If paused
        else if(gameScreen == GameScreen.PAUSED){
            handler.render(g);
            hud.render(g);
            menu.render(g);
        }

        // Only store rendered while in store
        else if(gameScreen == GameScreen.STORE){
            store.render(g);
        }

        // If in menu or at game over
        else if(gameScreen == GameScreen.WELCOME || gameScreen == GameScreen.INFO || gameScreen == GameScreen.GAME_OVER || gameScreen == GameScreen.SELECT_DIFFICULTY || gameScreen == GameScreen.VICTORY){
            handler.render(g);
            menu.render(g);
        }

        // Disposes the graphic of the previous render
        g.dispose();
        // Shows/renders the next buffer
        bufferStrategy.show();
    }

    /**
     * Clamping function for ints.
     * If the given parameter is below minimum, set to minimum.
     * If the given parameter is above maximum, set to maximum.
     * @param parameter the given parameter
     * @param min the desired lower threshold
     * @param max the desired upper threshold
     * @return the given parameter or the lower or upper threshold
     */
    public static int clamp(int parameter, int min, int max){
        // Kan også gjøres ved bruk av Math min&max, synes dette var mer visuelt lesbart.
        // Eventuell kilde for bruk av Clamp med Math funksjoner:
        // https://www.demo2s.com/java/java-math-clamp-int-val-int-min-int-max.html
        if(parameter >= max){
            return parameter = max;
        }
        else if(parameter <= min){
            return  parameter = min;
        }
        else {
            return parameter;
        }
    }

    /**
     * Sets the game screen
     * @param gameScreen of type GameScreen
     */
    public void setGameScreen(GameScreen gameScreen){
        this.gameScreen = gameScreen;
    }

    /**
     * Gets the game screen
     * @return object of type GameScreen
     */
    public GameScreen getGameScreen(){
        return gameScreen;
    }
}
