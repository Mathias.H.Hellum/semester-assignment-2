package inf101.opentask;

import inf101.opentask.entities.EntityID;
import inf101.opentask.entities.GameObject;
import java.awt.Graphics;
import java.util.ArrayList;

public class Handler {
    public ArrayList<GameObject> object = new ArrayList<>();
    // The initial movement speed/velocity of the player
    public int speed = 5;

    /**
     * The tick method for the Handler.
     * Ticks a copy of the ArrayList of GameObjects.
     */
    public void tick(){ //swapped to enhanced for loop here, can do same for below if this works.
        for (int i = 0; i < object.size(); i++) {
            GameObject tempObject = object.get(i);
            tempObject.tick();
        }
    }

    /**
     * The render method for the Handler.
     * Renders a copy of the ArrayList of GameObjects.
     * @param g
     */
    public void render(Graphics g){
        for (GameObject tempObject : object) {
            tempObject.render(g);
        }
    }

    /**
     * Adds a GameObject to the initial ArrayList of GameObjects
     * @param object the object to be added
     */
    public void addObject(GameObject object){
        this.object.add(object);
    }

    /**
     * Removes a GameObject from the initial ArrayList of GameObjects
     * @param object the object to be removed
     */
    public void removeObject(GameObject object){
        this.object.remove(object);
    }

    /**
     * Clears all entities except the player
     * <p>
     * An ArrayList of type GameObject tracking all objects, removes anything that does not have a player ID
     */
    public void clearGrunts() {
        ArrayList<GameObject> tempList = new ArrayList<>(object);
        for (GameObject tempObject : tempList) {
            if (tempObject.getId() != EntityID.PLAYER) {
                removeObject(tempObject);
            }
        }
    }
}