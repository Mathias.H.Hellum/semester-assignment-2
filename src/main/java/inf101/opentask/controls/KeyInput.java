package inf101.opentask.controls;

import inf101.opentask.Game;
import inf101.opentask.GameScreen;
import inf101.opentask.Handler;
import inf101.opentask.entities.EntityID;
import inf101.opentask.entities.GameObject;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Arrays;

public class KeyInput extends KeyAdapter {

    private final Handler handler;
    private final boolean[] keyDown = new boolean[4];
    private final Game game;

    /**
     * The constructor for KeyInput. Takes handle and game.
     * Handles player movement and general game logic based on users' key presses.
     * @param handler
     * @param game
     */
    public KeyInput(Handler handler, Game game){
        this.handler = handler;
        this.game = game;
        keyDown[0] = false;
        keyDown[1] = false;
        keyDown[2] = false;
        keyDown[3] = false;
    }

    // Native javadoc
    public void keyPressed(KeyEvent e){
        // NB!! Tatt inspirasjon fra bl.a følgende lenke for en metode som gir glatte bevegelser, forhindrer stopp ved brå bevegelser
        // https://gamedev.stackexchange.com/questions/68263/is-this-really-the-reason-for-smoother-movement

        // Movement in a directions sets a coordinate drift i.e. velocity, in that direction on keypress.
        // Key release stops the velocity.
        int key = e.getKeyCode();
        for (int i = 0; i < handler.object.size(); i++) {
            GameObject tempObject = handler.object.get(i);

            if(tempObject.getId() == EntityID.PLAYER){
                // Specifies which velocity to set and a positive or negative value
                if(key == KeyEvent.VK_W){
                    tempObject.setVelocityY(-handler.speed);
                    keyDown[0] = true;
                }
                if(key == KeyEvent.VK_S){
                    tempObject.setVelocityY(handler.speed);
                    keyDown[1] = true;
                }
                if(key == KeyEvent.VK_D){
                    tempObject.setVelocityX(handler.speed);
                    keyDown[2] = true;
                }
                if(key == KeyEvent.VK_A){
                    tempObject.setVelocityX(-handler.speed);
                    keyDown[3] = true;
                }
            }
        }

        // Pauses/unpauses the game
        if(key == KeyEvent.VK_SPACE){
            if(game.getGameScreen() == GameScreen.ACTIVE_GAME){
                game.setGameScreen(GameScreen.PAUSED);
            }
            else if(game.getGameScreen() == GameScreen.PAUSED){
                game.setGameScreen(GameScreen.ACTIVE_GAME);
            }
        }

        // Game exit on escape at any point
        if(key == KeyEvent.VK_ESCAPE) {
            System.exit(1);
        }

        // Enters/leaves the in-game store
        if(key == KeyEvent.VK_B){
            if(game.getGameScreen() == GameScreen.ACTIVE_GAME){
                game.setGameScreen(GameScreen.STORE);
            }
            else if(game.getGameScreen() == GameScreen.STORE){
                game.setGameScreen(GameScreen.ACTIVE_GAME);
            }
        }

    }

    // Native javadoc
    public void keyReleased(KeyEvent e){
        // forklaring i keyPressed over.
        int key = e.getKeyCode();
        for (int i = 0; i < handler.object.size(); i++) {
            GameObject tempObject = handler.object.get(i);

            if(tempObject.getId() == EntityID.PLAYER){
                if(key == KeyEvent.VK_W){
                    keyDown[0] = false;
                }
                if(key == KeyEvent.VK_S){
                    keyDown[1] = false;
                }
                if(key == KeyEvent.VK_D){
                    keyDown[2] = false;
                }
                if(key == KeyEvent.VK_A){
                    keyDown[3] = false;
                }

                //vertical movement
                if(!keyDown[0] && !keyDown[1]){
                    tempObject.setVelocityY(0);
                }
                //horizontal movement
                if(!keyDown[2] && !keyDown[3]){
                    tempObject.setVelocityX(0);
                }
            }
        }
    }

    // Kan brukes til testing.
    @Override
    public String toString() {
        return "KeyInput{" +
                "keyDown=" + Arrays.toString(keyDown) +
                '}';
    }
}
