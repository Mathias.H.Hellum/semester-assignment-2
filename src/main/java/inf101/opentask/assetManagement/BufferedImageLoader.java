package inf101.opentask.assetManagement;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;

public class BufferedImageLoader {

    private BufferedImage image;

    // En av kildene for å forstå bruken av BufferedImage for å kunne tegne sprites:
    // https://stackoverflow.com/questions/35638104/how-do-i-properly-create-a-bufferedimage-from-file

    // Også: ikke noe spesielt av tester å benytte for noen av klassene i assetManagement


    /**
     * Loads the image/sprite-sheet to be used from specified filepath
     * @param path the filepath of wanted image
     * @return image
     */
    public BufferedImage loadImage(String path){
        try {
            image = ImageIO.read(getClass().getResource(path));
        }
        catch (IOException e){
            e.printStackTrace();
        }
        return image;
    }
}
