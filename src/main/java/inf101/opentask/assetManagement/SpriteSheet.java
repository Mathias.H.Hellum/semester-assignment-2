package inf101.opentask.assetManagement;

import java.awt.image.BufferedImage;

public class SpriteSheet {

    private final BufferedImage bufferedImage;

    // Også: ikke noe spesielt av tester å benytte for noen av klassene i assetManagement


    /**
     * The constructor for SpriteSheet. Takes bufferedImage
     * @param bufferedImage
     */
    public SpriteSheet(BufferedImage bufferedImage){
        this.bufferedImage =bufferedImage;
    }

    /**
     * Gets the desired sprite from the sprite-sheet. Takes coordinate and size
     * @param row
     * @param col
     * @param width
     * @param height
     * @return
     */
    public BufferedImage getSprite(int row, int col, int width, int height){
        // " .getSubimage" gets whatever is within specified rectangular region.
        // Doing it like this enables easy use of a sprite-sheet with sprites of varying sizes,
        // as long as this is accounted for in the layout of the sprite-sheets themselves.
        return bufferedImage.getSubimage((col * 32) - 32, (row * 32) - 32, width, height);
    }
}
