package inf101.opentask.uiElements;

import inf101.opentask.Game;
import inf101.opentask.GameScreen;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

public class Menu {

    private final Game game;
    private final Hud hud;
    private final Font fontLarge = new Font("arial", Font.BOLD, 50);
    private final Font fontMedium = new Font("arial", Font.BOLD, 30);
    private final Font fontSmall = new Font("arial", Font.BOLD, 17);

    /**
     * The constructor for Menu. Takes game and hud.
     * @param game
     * @param hud
     */
    public Menu(Game game, Hud hud){
        this.game = game;
        this.hud = hud;
    }

    /**
     * Handles the rendering of the Menu
     * @param g
     */
    public void render(Graphics g){
        // Text at welcome
        if(game.getGameScreen() == GameScreen.WELCOME){

            g.setFont(fontLarge);
            g.setColor(Color.white);
            g.drawString("Menu", 120, 100);

            g.setFont(fontMedium);
            g.drawRect(80, 150, 200, 64);
            g.drawString("Play", 150, 190);

            g.drawRect(80, 250, 200, 64);
            g.drawString("Help", 150, 290);

            g.drawRect(80, 350, 200, 64);
            g.drawString("Quit", 150, 390);
        }

        // Text at info
        else if(game.getGameScreen() == GameScreen.INFO){

            g.setFont(fontLarge);
            g.setColor(Color.white);
            g.drawString("Info", 120, 100);

            g.setFont(fontSmall);
            g.drawString("Keybindings", 80, 140);
            g.drawString("WASD to move around and dodge enemies", 80, 160);
            g.drawString("Press Space to pause the game and B to enter store", 80, 180);
            g.drawString("Press Escape at any time to close the application", 80, 200);

            // Creative commons references
            g.setFont(fontSmall);
            g.drawString("Sound effects and bgm from creative commons 1.0 & 3.0",80, 260);
            g.drawString("Click sound: https://profiles.google.com/jun66le", 80, 280);
            g.drawString("Coin cling: https://freesound.org/people/BeezleFM/sounds/512133/", 80, 300);
            g.drawString("BGM: https://freesound.org/s/428858/", 80, 320);

            g.setFont(fontMedium);
            g.drawRect(80, 350, 200, 64);
            g.drawString("Back", 150, 390);
        }

        // Text at game over
        else if(game.getGameScreen() == GameScreen.GAME_OVER){

            g.setFont(fontLarge);
            g.setColor(Color.white);
            g.drawString("Game Over", 80, 100);

            g.setFont(fontMedium);
            g.drawString("You got to level: " + hud.getLevel(), 80, 200);
            g.drawString("With a score of: " + hud.getScore(), 80, 240);

            g.drawRect(80, 350, 200, 64);
            g.drawString("Try Again", 120, 390);
        }

        // Text at difficulty selection
        else if(game.getGameScreen() == GameScreen.SELECT_DIFFICULTY){

            g.setFont(fontLarge);
            g.setColor(Color.white);
            g.drawString("Difficulty", 80, 100);

            g.setFont(fontMedium);
            g.drawRect(80, 150, 200, 64);
            g.drawString("Normal", 150, 190);

            g.drawRect(80, 250, 200, 64);
            g.drawString("Hard", 150, 290);

            g.drawRect(80, 350, 200, 64);
            g.drawString("Back", 150, 390);
        }

        // Text at pause screen
        else if (game.getGameScreen() == GameScreen.PAUSED){

            g.setFont(fontLarge);
            g.setColor(Color.white);
            g.drawString("Paused", 300, 150);
        }

        // Text at victory screen
        else if (game.getGameScreen() == GameScreen.VICTORY){

            g.setFont(fontLarge);
            g.setColor(Color.white);
            g.drawString("Victory!", 90, 300);

            g.setFont(fontMedium);
            g.drawRect(80, 350, 200, 64);
            g.drawString("Play again", 110, 390);
        }
    }
}
