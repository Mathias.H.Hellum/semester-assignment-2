package inf101.opentask.uiElements;

import inf101.opentask.Game;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.util.Objects;

public class Hud {

    public int health = 100;
    public int shield;
    private int greenValue = 255;
    private int points;
    private int level = 1;

    /**
     * The tick function for Hud.
     * Handles score, health and shield.
     */
    public void tick(){
        health = Game.clamp(health, 0, 100);
        // Setting the rgb green value relative to health gives the option to
        // have a "darkening/reddening" health bar as it gets low.
        greenValue = health * 2;
        shield = Game.clamp(shield, 0, 100);
        // Score increases with every tick
        points++;
    }

    /**
     * The render function for Hud
     * @param g
     */
    public void render(Graphics g){
        // Backdrop for health bar
        g.setColor(Color.gray);
        g.fillRect(15, 15, 200, 25);

        // Health bar. New color every tick gives a redder health bar as it gets lower
        g.setColor(new Color(80, greenValue, 0));
        // Multiplied by 2 to always stay proportionate to the health bar
        g.fillRect(15, 15, health * 2, 25);
        g.setColor(Color.white);
        g.drawRect(15, 15, 200, 25);

        // Backdrop for armor bar
        g.setColor(Color.gray);
        g.fillRect(15, 50, 200, 25);
        // Armor bar
        g.setColor(Color.CYAN);
        g.fillRect(15, 50, shield * 2, 25);
        g.setColor(Color.white);
        g.drawRect(15, 50,200, 25);

        // Stat display
        g.drawString(health + "%", 100, 32);
        g.setFont(new Font("arial", Font.BOLD, 18));
        g.drawString("Score: " + points, 15, 100);
        g.drawString("Level: " + level, 15, 120);
    }

    /**
     * Getter for score
     * @return score
     */
    public int getScore(){
        return points;
    }

    /**
     * Setter for score
     * @param score
     */
    public void setScore(int score){
        this.points = score;
    }

    /**
     * Getter for level
     * @return level
     */
    public int getLevel(){
        return level;
    }

    /**
     * Setter for level
     * @param level
     */
    public void setLevel(int level){
        this.level = level;
    }


    // Kunne vært brukt til testing men ettersom det kun er snakk om enkeltverdier lar jeg være.
    // Ettersom samtlige verdier er festet med clamping vil det ikke brått bli "verdier på villspor"
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Hud)) return false;
        Hud hud = (Hud) o;
        return health == hud.health && shield == hud.shield && greenValue == hud.greenValue && points == hud.points && level == hud.level;
    }

    @Override
    public int hashCode() {
        return Objects.hash(health, shield, greenValue, points, level);
    }

    @Override
    public String toString() {
        return "Hud{" +
                "health=" + health +
                ", shield=" + shield +
                ", greenValue=" + greenValue +
                ", points=" + points +
                ", level=" + level +
                '}';
    }
}
