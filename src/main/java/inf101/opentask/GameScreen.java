package inf101.opentask;

public enum GameScreen {
    WELCOME,
    ACTIVE_GAME,
    GAME_OVER,
    PAUSED,
    SELECT_DIFFICULTY,
    STORE,
    INFO,
    VICTORY,
}
