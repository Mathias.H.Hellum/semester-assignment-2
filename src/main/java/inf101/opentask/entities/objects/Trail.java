package inf101.opentask.entities.objects;

import inf101.opentask.Handler;
import inf101.opentask.entities.EntityID;
import inf101.opentask.entities.GameObject;
import java.awt.Rectangle;
import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

public class Trail extends GameObject {

    // Kilder til å forstå hvordan lage trail effekter under i render
    private float alpha = 1;
    private final float life;
    private final Handler handler;
    private final Color color;
    private final int width, height;

    /**
     * The constructor for Trail. Takes spawn coordinates, ID, color, size, "life expectancy" and handler
     * @param x
     * @param y
     * @param entityId
     * @param color
     * @param width
     * @param height
     * @param life
     * @param handler
     */
    public Trail(int x, int y, EntityID entityId, Color color, int width, int height, float life, Handler handler) {
        super(x, y, entityId);
        this.handler = handler;
        this.color = color;
        this.width = width;
        this.height = height;
        this.life = life;
    }


    public void tick() {
        if(alpha > life){
            alpha -= (life - 0.0001f);
        }
        else
            handler.removeObject(this);
    }

    /**
     * Handles the rendering of the Trail
     * @param g
     */
    public void render(Graphics g) {
        // Noen kilder for å forstå hvordan lage en trail effect:
        // https://stackoverflow.com/questions/29932822/java-painting-program-how-to-leave-a-trail-behind-a-shape
        // https://www.tabnine.com/code/java/classes/java.awt.Graphics2D
        Graphics2D g2d = (Graphics2D) g;
        g2d.setComposite(makeTransparent(alpha));
        g.setColor(color);
        g.fillRect(x, y, width, height);
        g2d.setComposite(makeTransparent(1)); //fra vid 5, 18 min
    }

    // Ref linkene i render metoden over.
    private AlphaComposite makeTransparent(float alpha){
        int type = AlphaComposite.SRC_OVER;
        return AlphaComposite.getInstance(type, alpha);
    }

    /**
     * Handles the boundaries of the Trail
     * @return Coordinates and shape/size of type Rectangle
     */
    public Rectangle getBounds() {
        return null;
    }
}
