package inf101.opentask.entities.objects;

import inf101.opentask.Game;
import inf101.opentask.Handler;
import inf101.opentask.entities.EntityID;
import inf101.opentask.entities.GameObject;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.util.Random;

public class BossProjectile extends GameObject {

    private final Handler handler;

    /**
     * The constructor for BossProjectile. Takes spawn coordinates, ID and handler.
     * @param x
     * @param y
     * @param entityId
     * @param handler
     */
    public BossProjectile(int x, int y, EntityID entityId, Handler handler) {
        super(x, y, entityId);
        this.handler = handler;
        Random r = new Random();
        //Gives a random number from -5 to 5
        velocityX = r.nextInt(10) - 5; // Projectile trajectory/angle and horizontal speed
        velocityY = 5; // Vertical projectile speed
    }

    /**
     * Controls the movement pattern of projectiles. Gives them a trail.
     * Eventually removes the projectiles when they travel out of frame
     */
    public void tick() {
        x += velocityX;
        y += velocityY;

        // Removes the projectile when it reaches screen borders(regardless of trajectory),
        // done to reduce resource cost of an ever-growing number of out-of-frame projectiles.
        if(y > Game.HEIGHT || (x < 0 || x > Game.WIDTH)){
            handler.removeObject(this);
        }
        handler.addObject(new Trail(x, y, EntityID.TRAIL, Color.green, 16, 16, 0.05f, handler));
    }

    /**
     * Handles the rendering of the projectile
     * @param g Graphics
     */
    public void render(Graphics g) {
        g.setColor(Color.green);
        g.fillRect(x, y, 16, 16);
    }

    /**
     * Handles the boundaries of the projectile
     * @return Coordinates and shape/size of type Rectangle
     */
    public Rectangle getBounds() {
        return new Rectangle(x, y, 16, 16);
    }
}
