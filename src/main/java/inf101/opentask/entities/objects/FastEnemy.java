package inf101.opentask.entities.objects;

import inf101.opentask.Game;
import inf101.opentask.Handler;
import inf101.opentask.assetManagement.SpriteSheet;
import inf101.opentask.entities.EntityID;
import inf101.opentask.entities.GameObject;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;

public class FastEnemy extends GameObject {

    private final BufferedImage entityImage;

    /**
     * The constructor for FastEnemy. Takes spawn coordinates, ID and handler.
     * Also grabs desired sprite.
     * @param x
     * @param y
     * @param entityId
     * @param handler
     */
    public FastEnemy(int x, int y, EntityID entityId, Handler handler) {
        super(x, y, entityId);
        velocityX = 10;
        velocityY = 10;
        SpriteSheet spriteSheet = new SpriteSheet(Game.spriteSheet);
        entityImage = spriteSheet.getSprite(1, 3, 16, 16);
    }

    /**
     * Controls the movement pattern of the FastEnemy.
     * Inverts velocity when hitting game borders.
     */
    public void tick() {
        x += velocityX;
        y += velocityY;
        if(y <= 0 || y >= Game.HEIGHT - 48) velocityY *= -1;
        if(x <= 0 || x >= Game.WIDTH - 32) velocityX *= -1;
    }

    /**
     * Handles the sprite rendering of the FastEnemy
     * @param g
     */
    public void render(Graphics g) {
        g.drawImage(entityImage, x, y, 16, 16, null);
    }

    /**
     * Handles the boundaries of the FastEnemy
     * @return Coordinates and shape/size of type Rectangle
     */
    public Rectangle getBounds() {
        return new Rectangle(x, y, 16, 16);
    }
}
