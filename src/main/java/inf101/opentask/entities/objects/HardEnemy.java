package inf101.opentask.entities.objects;

import inf101.opentask.Game;
import inf101.opentask.Handler;
import inf101.opentask.assetManagement.SpriteSheet;
import inf101.opentask.entities.EntityID;
import inf101.opentask.entities.GameObject;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.util.Random;

public class HardEnemy extends GameObject {

    private final BufferedImage entityImage;
    private final Random r = new Random();

    /**
     * The constructor for HardEnemy. Takes spawn coordinates, ID and handler.
     * Also grabs desired sprite.
     * @param x
     * @param y
     * @param entityId
     * @param handler
     */
    public HardEnemy(int x, int y, EntityID entityId, Handler handler) {
        super(x, y, entityId);
        velocityX = 5;
        velocityY = 5;
        SpriteSheet spriteSheet = new SpriteSheet(Game.spriteSheet);
        entityImage = spriteSheet.getSprite(2, 2, 16, 16);
    }

    /**
     * Controls the movement pattern of the HardEnemy.
     * Inverts velocity by a random set amount when hitting game borders.
     */
    public void tick() {
        x += velocityX;
        y += velocityY;
        // (the movement of this enemy sometimes bugs out when hitting game borders)
        // Inverts the entities' speed but with a randomized velocity.

        // NB!! Matten bak denne tilfeldiggjøringen av hastighet fikk jeg hjelp til av en venn utenfor studiet.

        if(x <= 0 || x >= Game.WIDTH - 32){
            if(velocityX <= 0){
                velocityX = -(r.nextInt(10) + 1) * -1;
            }
            else {
                velocityX = (r.nextInt(10) + 1) * -1;
            }
        }
        if(y <= 0 || y >= Game.HEIGHT - 48){
            if (velocityY <= 0){
                velocityY = -(r.nextInt(10) + 1) * -1;
            }
            else {
                velocityY = (r.nextInt(10) + 1) * -1;
            }
        }
    }

    /**
     * Handles the sprite rendering of the HardEnemy
     * @param g
     */
    public void render(Graphics g) {
        g.drawImage(entityImage, x, y, null);
    }

    /**
     * Handles the boundaries of the HardEnemy
     * @return Coordinates and shape/size of type Rectangle
     */
    public Rectangle getBounds() {
        return new Rectangle(x, y, 16, 16);
    }
}
