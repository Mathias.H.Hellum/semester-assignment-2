package inf101.opentask.entities.objects;

import inf101.opentask.Game;
import inf101.opentask.Handler;
import inf101.opentask.assetManagement.SpriteSheet;
import inf101.opentask.entities.EntityID;
import inf101.opentask.entities.GameObject;
import inf101.opentask.uiElements.Hud;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;

public class Player extends GameObject {

    private final Handler handler;
    private final Hud hud;
    private final BufferedImage entityImage;

    /**
     * The constructor for Player. Takes spawn coordinates, ID, handler and hud.
     * Also grabs desired sprite.
     * @param x
     * @param y
     * @param entityId
     * @param handler
     * @param hud
     */
    public Player(int x, int y, EntityID entityId, Handler handler, Hud hud){
        super(x, y, entityId);
        this.handler = handler;
        this.hud = hud;
        SpriteSheet spriteSheet = new SpriteSheet(Game.spriteSheet);
        entityImage = spriteSheet.getSprite(1, 1, 32, 32);
    }

    /**
     * For testing only
     * @param player
     * @param handler
     * @param hud
     * @param entityImage
     */
    public Player(EntityID player, Handler handler, Hud hud, BufferedImage entityImage) {
        super(player);
        this.handler = handler;
        this.hud = hud;
        this.entityImage = entityImage;
    }

    /**
     * Controls the allowed movement of the player.
     * Also calls on the collisionCheck method.
     */
    public void tick() {
        x += velocityX;
        y += velocityY;
        // border coordinate limits for the player. Magic numbers hard to avoid due to sprites having a fixed size
        x = Game.clamp(x, 0, Game.WIDTH - 47);
        y = Game.clamp(y, 0, Game.HEIGHT - 70);
        collisionCheck();
    }

    /**
     * Tracks collision between the player and all enemies
     */
    public void collisionCheck(){
        // NB!! bruker metoder som bl.a ".intersects" som kommer fra Javas Rectangle klasse, min kilde til å forstå dette:
        // https://www.youtube.com/watch?v=qIr2XYZrznI&ab_channel=BroCode
        for (int i = 0; i < handler.object.size(); i++) {
            GameObject tempObject = handler.object.get(i);

            // Colliding with all grunts except HardEnemy. Steadily drains player health
            if (tempObject.getId() == EntityID.BASIC_ENEMY || tempObject.getId() == EntityID.FAST_ENEMY || tempObject.getId() == EntityID.SMART_ENEMY || tempObject.getId() == EntityID.BOSS_PROJECTILE){
                if (getBounds().intersects(tempObject.getBounds())) {
                    // What happens:
                    if(hud.shield > 0){
                        hud.shield -= 2;
                    }
                    else {
                        hud.health -= 2;
                    }
                }
            }
            // Colliding with HardEnemy. Ignore shields and drains player health 50% faster than regular grunts.
            if (tempObject.getId() == EntityID.HARD_ENEMY){
                if(getBounds().intersects(tempObject.getBounds())){
                    // What happens
                    hud.health -= 3;
                }
            }
            // Colliding with a boss. Rapidly drains player health
            if (tempObject.getId() == EntityID.BOSS) {
                if (getBounds().intersects(tempObject.getBounds())) {
                    // What happens:
                    if(hud.shield > 0){
                        hud.shield -= 10;
                    }
                    else{
                        hud.health -= 10;
                    }
                }
            }
        }
    }

    /**
     * Handles the sprite rendering of the Player
     * @param g
     */
    public void render(Graphics g) {
        g.drawImage(entityImage, x, y, null);
    }

    /**
     * Handles the boundaries of the Player
     * @return Coordinates and shape/size of type Rectangle
     */
    public Rectangle getBounds(){
        return new Rectangle(x, y, 32, 32);
    }
}
