package inf101.opentask.entities.objects;

import inf101.opentask.Game;
import inf101.opentask.Handler;
import inf101.opentask.assetManagement.SpriteSheet;
import inf101.opentask.entities.EntityID;
import inf101.opentask.entities.GameObject;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;

public class SmartEnemy extends GameObject {

    private GameObject player;
    private final BufferedImage entityImage;

    /**
     * The constructor for SmartEnemy. Takes spawn coordinates, ID and handler.
     * Also grabs desired sprite & tracks player IDs.
     *
     * @param x
     * @param y
     * @param entityId
     * @param handler
     */
    public SmartEnemy(int x, int y, EntityID entityId, Handler handler) {
        super(x, y, entityId);
        // Continuously checks all the entities present to find the player.
        for (int i = 0; i < handler.object.size(); i++) {
            if(handler.object.get(i).getId() == EntityID.PLAYER){
                player = handler.object.get(i);
            }
        }
        SpriteSheet spriteSheet = new SpriteSheet(Game.spriteSheet);
        entityImage = spriteSheet.getSprite(2, 1, 16, 16);
    }

    /**
     * For testing only
     * @param entityID
     * @param entityImage
     */
    public SmartEnemy(EntityID entityID, BufferedImage entityImage){
        super((entityID));
        this.entityImage = entityImage;
    }

    /**
     * Controls the movement pattern of SmartEnemy.
     * <p>
     * Follows the player
     */
    public void tick() {
        x += velocityX;
        y += velocityY;
        // X & Y distance between the SmartEnemy and the player
        float distanceX = x - player.getX() - 8; // - 8 to "move towards center of the player object and not its coordinates which are top left corner"
        float distanceY = y - player.getY() - 8; // - 8 to "move towards center of the player object and not its coordinates which are top left corner"


        // NB!! Matten bak de følgende formlene fikk jeg hjelp til av en venn utenfor studiet.

        float distance = (float) Math.sqrt(((x - player.getX()) * (x - player.getX())) + ((y - player.getY()) * (y - player.getY())));
        // As velocities are cast to int, multiplying by more than 1 at the end avoids the risk of the int being 0 and thus stands still.
        velocityX = (int) ((-1.0 / distance) * distanceX*4); // "* distanceX" at end increases velocityX
        velocityY = (int) ((-1.0 / distance) * distanceY*4); // "* distanceY" at end increases velocityY
    }

    /**
     * Handles the sprite rendering of the SmartEnemy
     * @param g
     */
    public void render(Graphics g) {
        g.drawImage(entityImage, x, y, null);
    }

    /**
     * Handles the boundaries of the SmartEnemy
     * @return Coordinates and shape/size of type Rectangle
     */
    public Rectangle getBounds() {
        return new Rectangle(x, y, 16, 16);
    }
}
