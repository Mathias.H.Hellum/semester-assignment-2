package inf101.opentask.entities.objects;

import inf101.opentask.Game;
import inf101.opentask.Handler;
import inf101.opentask.assetManagement.SpriteSheet;
import inf101.opentask.entities.EntityID;
import inf101.opentask.entities.GameObject;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.util.Random;

public class Boss extends GameObject {

    private final Handler handler;
    private final Random r = new Random();
    private final BufferedImage entityImage;
    // How far down the boss moves before it starts strafing
    private int timer = Game.HEIGHT / 9;

    /**
     * The constructor for the Boss. Takes spawn coordinates, ID and handler.
     * Also grabs desired sprite.
     * @param x
     * @param y
     * @param entityId
     * @param handler
     */
    public Boss(int x, int y, EntityID entityId, Handler handler) {
        super(x, y, entityId);
        this.handler = handler;
        velocityX = 0;
        velocityY = 2;
        SpriteSheet spriteSheet = new SpriteSheet(Game.spriteSheetLarge);
        entityImage = spriteSheet.getSprite(1, randomBoss(), 96, 96);
    }

    /**
     * Cosmetic function used in giving the boss a random appearance.
     * Values are only applicable to large sprite-sheets
     * @return 1 or 4
     */
    public int randomBoss(){
        int rng;
        int temp = r.nextInt(5);
        if(temp>2){
            rng = 1;
        }
        else {
            rng = 4;
        }
        return rng;
    }

    /**
     * Controls the movement pattern of the boss, also calls on BossProjectile
     */
    public void tick() {
        x += velocityX;
        y += velocityY;

        // Stops the initial vertical velocity of the boss after coming into frame
        if(timer < 0){
            velocityY = 0;
            if(velocityX == 0){
                // Initiates horizontal velocity of entity.
                velocityX = 2;
            }
            int projectiles = r.nextInt(8); // Larger number = less projectiles
            if(projectiles == 0){
                handler.addObject(new BossProjectile(x + 48, y + 90, EntityID.BOSS_PROJECTILE, handler));
            }
        }
        else{
            timer--;
        }
        // Reverses the horizontal velocity by equal amount when the entity hits game borders.
        if(x <= 0 || x >= Game.WIDTH - 112){ // difficult to avoid "magic numbers" as Sprites are not size adjustable
            velocityX *= -1;
        }
    }

    /**
     * Handles the sprite rendering of the boss
     * @param g
     */
    public void render(Graphics g) {
        g.drawImage(entityImage, x, y, null);
    }

    /**
     * Handles the boundaries of the boss
     * @return Coordinates and shape/size of type Rectangle
     */
    public Rectangle getBounds() {
        return new Rectangle(x, y, 96, 96);
    }
}
