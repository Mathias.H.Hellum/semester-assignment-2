package inf101.opentask.entities;

import java.awt.Graphics;
import java.awt.Rectangle;

public abstract class GameObject {

    /*
    NB!! Jeg er klar over at jeg sikkert kunne hatt de forskjellige objektene (player & enemeies) arve fra -
    en felles klasse, men "forsvarer" denne strukturen med at jeg enkelt kan legge til mer
    unik funksjonalitet slik det er satt opp nå, det er ihvertfall min tankegang.
    */

    // Made protected so they are available to child classes
    protected int x, y;
    protected int velocityX, velocityY;
    protected EntityID entityId;

    /**
     * The constructor for the abstract class GameObject. Takes spawn coordinates and ID.
     * @param x
     * @param y
     * @param entityId
     */
    public GameObject(int x, int y, EntityID entityId){
        this.x = x;
        this.y = y;
        this.entityId = entityId;
    }

    /**
     * For testing
     * @param entityId
     */
    public GameObject(EntityID entityId){
        this.entityId = entityId;
    }

    /**
     * Controls the movement of the entity.
     * Also adds possible functions of entities.
     */
    public abstract void tick();

    /**
     * Handles the rendering of the entity
     * @param g
     */
    public abstract void render(Graphics g);

    /**
     * Handles the boundaries of the entity
     * @return Coordinates and shape/size of type Rectangle
     */
    public abstract Rectangle getBounds();

    /**
     * Gets the x value
     * @return the x value
     */
    public int getX(){
        return x;
    }

    /**
     * Gets the y value
     * @return the y value
     */
    public int getY(){
        return y;
    }

    /**
     * Sets the x value
     * @param x
     */
    public void setX(int x){
        this.x = x;
    }

    /**
     * Sets the y value
     * @param y
     */
    public void setY(int y){
        this.y = y;
    }

    /**
     * Gets the EntityID
     * @return the EntityID
     */
    public EntityID getId(){
        return entityId;
    }

    /**
     * Sets the EntityID
     * @param entityId
     */
    public void setId(EntityID entityId){
        // Klar over at bl.a. denne ikke er i bruk, men tenkte den var fin til potensiell bruk.
        // Use case: en enemy kan endre ID i løpet av spillet og dermed -
        // "interkativt" få en annen / vanskeligere oppførsel å hanskes med for spilleren.
        this.entityId = entityId;
    }

    /**
     * Gets the velocity of x
     * @return velocity of x
     */
    public int getVelocityX(){
        // Samme forsvar som for setId over
        return velocityX;
    }

    /**
     * Gets the velocity of y
     * @return velocity of y
     */
    public int getVelocityY(){
        // samme forsvar som for setId over
        return velocityY;
    }

    /**
     * Sets the velocity of x
     * @param velocityX
     */
    public void setVelocityX(int velocityX){
        this.velocityX = velocityX;
    }

    /**
     * Sets the velocity of y
     * @param velocityY
     */
    public void setVelocityY(int velocityY){
        this.velocityY = velocityY;
    }
}
