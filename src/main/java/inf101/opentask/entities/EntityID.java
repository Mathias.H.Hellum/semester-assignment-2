package inf101.opentask.entities;

/**
 * Enum IDs for all entities
 */
public enum EntityID {
    PLAYER(),
    BASIC_ENEMY(),
    HARD_ENEMY(),
    FAST_ENEMY(),
    SMART_ENEMY(),
    BOSS(),
    BOSS_PROJECTILE(),
    MENU_PARTICLE(),
    TRAIL(),
}
